package edu.cmu.spareddy.xray

class DOMNode() {
    var text: String? = null
    var description: String? = null
    var className: String? = null
    var children = arrayListOf<DOMNode>()
}